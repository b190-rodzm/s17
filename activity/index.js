/* 1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	// first function here:
function getProfile (){
  let fullName = prompt("Please enter your full name.");
  let age = prompt("Enter your age.");
  let currentLocation = prompt ("Enter your current address.");
  alert ("Thank you for your answers!")
  console.log("Hello, "+fullName);
  console.log("You are"+" "+age+" "+"years old.");
  console.log("You live in "+currentLocation);
};
getProfile ();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
  function musicArtistList (){
    console.log("1. Coldplay");
    console.log("2. Ed Sheeran");
    console.log("3. Harry Styles");
    console.log("4. The Weekend");
    console.log("5. BTS");
  };
  musicArtistList ();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	// third function here:
  function moviesList (){
    console.log("1. Inception");
    console.log("Rotten Tomatoes Rating: 87%");
    console.log("2. The Dark Knight");
    console.log("Rotten Tomatoes Rating: 94%");
    console.log("3. Star Wars: The Force Awakens");
    console.log("Rotten Tomatoes Rating: 93%");
    console.log("4. Avengers: Endgame");
    console.log("Rotten Tomatoes Rating: 94%");
    console.log("5. The Shawshank Redemption");
    console.log("Rotten Tomatoes Rating: 91%");
  };
  moviesList();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
  alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();

